/*
 * ************************************************************************
 *                  Université de Nice Sophia-Antipolis (UNS) -
 *                  Centre National de la Recherche Scientifique (CNRS)
 *                  Copyright © 2016 UNS, CNRS
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *
 *     Author: Cyril Cecchinel – Laboratoire I3S – cecchine@i3s.unice.fr
 * ***********************************************************************
 */

// License: GPL. For details, see Readme.txt file.
package fr.unice.iut.info.methodo.maps;

import java.util.ArrayList;
import java.util.List;

public class AbstractLayer {
    private LayerGroup parent;
    private String name;
    private String description;
    private Style style;
    private Boolean visible;
    private Boolean visibleTexts = Boolean.TRUE;

    public AbstractLayer(String name) {
        this(name, (String) null);
    }

    public AbstractLayer(String name, String description) {
        this(name, description, MapMarkerCircle.getDefaultStyle());
    }

    public AbstractLayer(String name, Style style) {
        this(name, null, style);
    }

    public AbstractLayer(String name, String description, Style style) {
        this(null, name, description, style);
    }

    public AbstractLayer(LayerGroup parent, String name) {
        this(parent, name, MapMarkerCircle.getDefaultStyle());
    }

    public AbstractLayer(LayerGroup parent, String name, Style style) {
        this(parent, name, null, style);
    }

    public AbstractLayer(LayerGroup parent, String name, String description, Style style) {
        setParent(parent);
        setName(name);
        setDescription(description);
        setStyle(style);
        setVisible(Boolean.TRUE);

        if (parent != null) parent.add(this);
    }

    public LayerGroup getParent() {
        return parent;
    }

    public void setParent(LayerGroup parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public Boolean isVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public static <E> List<E> add(List<E> list, E element) {
        if (element != null) {
            if (list == null) list = new ArrayList<>();
            if (!list.contains(element)) list.add(element);
        }
        return list;
    }

    public Boolean isVisibleTexts() {
        return visibleTexts;
    }

    public void setVisibleTexts(Boolean visibleTexts) {
        this.visibleTexts = visibleTexts;
    }

    @Override
    public String toString() {
        return name;
    }
}
