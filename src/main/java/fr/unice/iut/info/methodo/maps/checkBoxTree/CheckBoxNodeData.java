/*
 * ************************************************************************
 *                  Université de Nice Sophia-Antipolis (UNS) -
 *                  Centre National de la Recherche Scientifique (CNRS)
 *                  Copyright © 2016 UNS, CNRS
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *
 *     Author: Cyril Cecchinel – Laboratoire I3S – cecchine@i3s.unice.fr
 * ***********************************************************************
 */

// License: GPL. For details, see Readme.txt file.
package fr.unice.iut.info.methodo.maps.checkBoxTree;

import fr.unice.iut.info.methodo.maps.AbstractLayer;
import fr.unice.iut.info.methodo.maps.LayerGroup;

/**
 * Node Data for checkBox Tree
 *
 * @author galo
 */
public class CheckBoxNodeData {
    private AbstractLayer layer;

    public CheckBoxNodeData(final AbstractLayer layer) {
        this.layer = layer;
    }

    public CheckBoxNodeData(final String txt) {
        this(new LayerGroup(txt));
    }

    public CheckBoxNodeData(final String txt, final Boolean selected) {
        this(new LayerGroup(txt));
        layer.setVisible(selected);
    }

    public Boolean isSelected() {
        return layer.isVisible();
    }

    public void setSelected(final Boolean newValue) {
        layer.setVisible(newValue);
    }

    public String getText() {
        return layer.getName();
    }

    public AbstractLayer getAbstractLayer() {
        return layer;
    }

    public void setAbstractLayer(final AbstractLayer layer) {
        this.layer = layer;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '[' + getText() + '/' + isSelected() + ']';
    }
}
