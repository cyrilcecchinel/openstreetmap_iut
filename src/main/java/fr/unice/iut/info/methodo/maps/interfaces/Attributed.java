/*
 * ************************************************************************
 *                  Université de Nice Sophia-Antipolis (UNS) -
 *                  Centre National de la Recherche Scientifique (CNRS)
 *                  Copyright © 2016 UNS, CNRS
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *
 *     Author: Cyril Cecchinel – Laboratoire I3S – cecchine@i3s.unice.fr
 * ***********************************************************************
 */

// License: GPL. For details, see Readme.txt file.
package fr.unice.iut.info.methodo.maps.interfaces;

import java.awt.Image;

public interface Attributed {
    /**
     * @return True if the tile source requires attribution in text or image form.
     */
    boolean requiresAttribution();

    /**
     * @param zoom The optional zoom level for the view.
     * @param botRight The bottom right of the bounding box for attribution.
     * @param topLeft The top left of the bounding box for attribution.
     * @return Attribution text for the image source.
     */
    String getAttributionText(int zoom, ICoordinate topLeft, ICoordinate botRight);

    /**
     * @return The URL to open when the user clicks the attribution text.
     */
    String getAttributionLinkURL();

    /**
     * @return The URL for the attribution image. Null if no image should be displayed.
     */
    Image getAttributionImage();

    /**
     * @return The URL to open when the user clicks the attribution image.
     * When return value is null, the image is still displayed (provided getAttributionImage()
     * returns a value other than null), but the image does not link to a website.
     */
    String getAttributionImageURL();

    /**
     * @return The attribution "Terms of Use" text.
     * In case it returns null, but getTermsOfUseURL() is not null, a default
     * terms of use text is used.
     */
    String getTermsOfUseText();

    /**
     * @return The URL to open when the user clicks the attribution "Terms of Use" text.
     */
    String getTermsOfUseURL();
}
