/*
 * ************************************************************************
 *                  Université de Nice Sophia-Antipolis (UNS) -
 *                  Centre National de la Recherche Scientifique (CNRS)
 *                  Copyright © 2016 UNS, CNRS
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *
 *     Author: Cyril Cecchinel – Laboratoire I3S – cecchine@i3s.unice.fr
 * ***********************************************************************
 */

// License: GPL. For details, see Readme.txt file.
package fr.unice.iut.info.methodo.maps;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;

import fr.unice.iut.info.methodo.maps.interfaces.MapRectangle;

public class MapRectangleImpl extends MapObjectImpl implements MapRectangle {

    private Coordinate topLeft;
    private Coordinate bottomRight;

    public MapRectangleImpl(Coordinate topLeft, Coordinate bottomRight) {
        this(null, null, topLeft, bottomRight);
    }

    public MapRectangleImpl(String name, Coordinate topLeft, Coordinate bottomRight) {
        this(null, name, topLeft, bottomRight);
    }

    public MapRectangleImpl(Layer layer, Coordinate topLeft, Coordinate bottomRight) {
        this(layer, null, topLeft, bottomRight);
    }

    public MapRectangleImpl(Layer layer, String name, Coordinate topLeft, Coordinate bottomRight) {
        this(layer, name, topLeft, bottomRight, getDefaultStyle());
    }

    public MapRectangleImpl(Layer layer, String name, Coordinate topLeft, Coordinate bottomRight, Style style) {
        super(layer, name, style);
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
    }

    @Override
    public Coordinate getTopLeft() {
        return topLeft;
    }

    @Override
    public Coordinate getBottomRight() {
        return bottomRight;
    }

    @Override
    public void paint(Graphics g, Point topLeft, Point bottomRight) {
        // Prepare graphics
        Color oldColor = g.getColor();
        g.setColor(getColor());
        Stroke oldStroke = null;
        if (g instanceof Graphics2D) {
            Graphics2D g2 = (Graphics2D) g;
            oldStroke = g2.getStroke();
            g2.setStroke(getStroke());
        }
        // Draw
        g.drawRect(topLeft.x, topLeft.y, bottomRight.x - topLeft.x, bottomRight.y - topLeft.y);
        // Restore graphics
        g.setColor(oldColor);
        if (g instanceof Graphics2D) {
            ((Graphics2D) g).setStroke(oldStroke);
        }
        int width = bottomRight.x-topLeft.x;
        int height = bottomRight.y-topLeft.y;
        Point p = new Point(topLeft.x+(width/2), topLeft.y+(height/2));
        if (getLayer() == null || getLayer().isVisibleTexts()) paintText(g, p);
    }

    public static Style getDefaultStyle() {
        return new Style(Color.BLUE, null, new BasicStroke(2), getDefaultFont());
    }

    @Override
    public String toString() {
        return "MapRectangle from " + getTopLeft() + " to " + getBottomRight();
    }
}
