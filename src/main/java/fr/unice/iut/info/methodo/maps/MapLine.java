/*
 * ************************************************************************
 *                  Université de Nice Sophia-Antipolis (UNS) -
 *                  Centre National de la Recherche Scientifique (CNRS)
 *                  Copyright © 2016 UNS, CNRS
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *
 *     Author: Cyril Cecchinel – Laboratoire I3S – cecchine@i3s.unice.fr
 * ***********************************************************************
 */

package fr.unice.iut.info.methodo.maps;

/**
 * Created by blay on 26/08/2016.
 *
 */
// License: GPL. For details, see Readme.txt file.

import fr.unice.iut.info.methodo.maps.interfaces.ICoordinate;

import java.util.*;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Stroke;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MapLine extends MapObjectImpl {

    private final static Logger LOGGER = Logger.getLogger(MapLine.class.getName());

    private List<? extends ICoordinate> lineCoordinates;

    public MapLine(ICoordinate ... points) {
        this(null, null, points);
    }

    public MapLine(List<? extends ICoordinate> points) {
        this(null, null, points);
    }

    public MapLine(String name, List<? extends ICoordinate> points) {
        this(null, name, points);
    }

    public MapLine(String name, ICoordinate ... points) {
        this(null, name, points);
    }

    public MapLine(Layer layer, List<? extends ICoordinate> points) {
        this(layer, null, points);
    }

    public MapLine(Layer layer, String name, List<? extends ICoordinate> points) {
        this(layer, name, points, getDefaultStyle());
    }

    public MapLine(Layer layer, String name, ICoordinate ... points) {
        this(layer, name, Arrays.asList(points), getDefaultStyle());
    }

    public MapLine(Layer layer, String name, List<? extends ICoordinate> points, Style style) {
        super(layer, name, style);
        this.lineCoordinates = points;
    }

    public List<? extends ICoordinate> getlineCoordinates() {
        return this.lineCoordinates;
    }

    public void paint(Graphics g, JMapViewer jMapViewer) {
        List<Point> points = new ArrayList<>();
        for(ICoordinate c : lineCoordinates)
        {
           points.add(jMapViewer.getMapPosition(c.getLat(), c.getLon()));
        }
        paint(g,points);
    }

    public void paint(Graphics g, List<Point> points) {
        if ( (points == null) || (points.isEmpty())) {
            return;
        }
        if (points.size()==1 ) {
            LOGGER.log(Level.WARNING,"list reduced to one point : " + points);
            return;
        }

        // Prepare graphics
       Color oldColor = g.getColor();
        g.setColor(getColor());


        // Affecte l'épaisseur du trait.... why?
        Stroke oldStroke = null;
        if (g instanceof Graphics2D) {
            Graphics2D g2 = (Graphics2D) g;
            oldStroke = g2.getStroke();
            g2.setStroke(getStroke());
        }

        // Draw

        Iterator iter = points.iterator();
        Point depart = (Point) iter.next();
        while (iter.hasNext() && (depart == null) ){
            LOGGER.log(Level.WARNING, "mapList contains null points !!");
            depart = (Point) iter.next();
        }

        while (iter.hasNext())
        {
            Point p = (Point) iter.next();
            if (p==null) {
                LOGGER.log(Level.WARNING, "mapList contains null points : abort !!");
                return;
            }
            else {
                //System.out.println("affichage .. d'une ligne de " + depart + " à " + p);
                g.drawLine(depart.x, depart.y, p.x, p.y);
                depart = p;
            }
        }


        // Restore graphics
        g.setColor(oldColor);
        if (g instanceof Graphics2D) {
            ((Graphics2D) g).setStroke(oldStroke);
        }

    }

    public static Style getDefaultStyle() {
        return new Style(Color.BLUE, new Color(100, 100, 100, 50), new BasicStroke(2), getDefaultFont());
    }

    @Override
    public String toString() {
        return "MapPolygon [points=" + lineCoordinates + ']';
    }
}
