/*
 * ************************************************************************
 *                  Université de Nice Sophia-Antipolis (UNS) -
 *                  Centre National de la Recherche Scientifique (CNRS)
 *                  Copyright © 2016 UNS, CNRS
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *
 *     Author: Cyril Cecchinel – Laboratoire I3S – cecchine@i3s.unice.fr
 * ***********************************************************************
 */

// License: GPL. For details, see Readme.txt file.
package fr.unice.iut.info.methodo.maps.tilesources;

import java.awt.Image;

import fr.unice.iut.info.methodo.maps.interfaces.ICoordinate;

/**
 * Abstract class for OSM Tile sources
 */
public abstract class AbstractOsmTileSource extends TMSTileSource {

    /**
     * The OSM attribution. Must be always in line with
     * <a href="https://www.openstreetmap.org/copyright/en">https://www.openstreetmap.org/copyright/en</a>
     */
    public static final String DEFAULT_OSM_ATTRIBUTION = "\u00a9 OpenStreetMap contributors";

    /**
     * Constructs a new OSM tile source
     * @param name Source name as displayed in GUI
     * @param baseUrl Source URL
     * @param id unique id for the tile source; contains only characters that
     * are safe for file names; can be null
     */
    public AbstractOsmTileSource(String name, String baseUrl, String id) {
        super(new TileSourceInfo(name, baseUrl, id));
    }

    @Override
    public int getMaxZoom() {
        return 19;
    }

    @Override
    public boolean requiresAttribution() {
        return true;
    }

    @Override
    public String getAttributionText(int zoom, ICoordinate topLeft, ICoordinate botRight) {
        return DEFAULT_OSM_ATTRIBUTION;
    }

    @Override
    public String getAttributionLinkURL() {
        return "https://openstreetmap.org/";
    }

    @Override
    public Image getAttributionImage() {
        return null;
    }

    @Override
    public String getAttributionImageURL() {
        return null;
    }

    @Override
    public String getTermsOfUseText() {
        return null;
    }

    @Override
    public String getTermsOfUseURL() {
        return "https://www.openstreetmap.org/copyright";
    }
}
