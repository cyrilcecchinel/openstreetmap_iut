/*
 * ************************************************************************
 *                  Université de Nice Sophia-Antipolis (UNS) -
 *                  Centre National de la Recherche Scientifique (CNRS)
 *                  Copyright © 2016 UNS, CNRS
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *
 *     Author: Cyril Cecchinel – Laboratoire I3S – cecchine@i3s.unice.fr
 * ***********************************************************************
 */

// License: GPL. For details, see Readme.txt file.
package fr.unice.iut.info.methodo.maps;

import java.util.List;

public class LayerGroup extends AbstractLayer {
    private List<AbstractLayer> layers;

    public LayerGroup(String name) {
        super(name);
    }

    public LayerGroup(String name, String description) {
        super(name, description);
    }

    public LayerGroup(String name, Style style) {
        super(name, style);
    }

    public LayerGroup(String name, String description, Style style) {
        super(name, description, style);
    }

    public LayerGroup(LayerGroup parent, String name) {
        super(parent, name);
    }

    public LayerGroup(LayerGroup parent, String name, String description, Style style) {
        super(name, description, style);
    }

    public List<AbstractLayer> getLayers() {
        return layers;
    }

    public void setElements(List<AbstractLayer> layers) {
        this.layers = layers;
    }

    public Layer addLayer(String name) {
        Layer layer = new Layer(this, name);
        layers = add(layers, layer);
        return layer;
    }

    public LayerGroup add(AbstractLayer layer) {
        layer.setParent(this);
        layers = add(layers, layer);
        return this;
    }

    public void calculateVisibleTexts() {
        Boolean calculate = null;
        if (layers != null && !layers.isEmpty()) {
            calculate = layers.get(0).isVisibleTexts();
            for (int i = 1; i < layers.size(); i++) {
                calculate = resultOf(calculate, layers.get(i).isVisibleTexts());
            }
        }
        setVisibleTexts(calculate);
        if (getParent() != null) getParent().calculateVisibleTexts();
    }

    public Boolean resultOf(Boolean b1, Boolean b2) {
        if (b1 != null && b1.equals(b2)) {
            return b1;
        }
        return Boolean.FALSE;
    }
}
