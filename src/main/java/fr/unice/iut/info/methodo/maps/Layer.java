/*
 * ************************************************************************
 *                  Université de Nice Sophia-Antipolis (UNS) -
 *                  Centre National de la Recherche Scientifique (CNRS)
 *                  Copyright © 2016 UNS, CNRS
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *
 *     Author: Cyril Cecchinel – Laboratoire I3S – cecchine@i3s.unice.fr
 * ***********************************************************************
 */

// License: GPL. For details, see Readme.txt file.
package fr.unice.iut.info.methodo.maps;

import java.util.List;

import fr.unice.iut.info.methodo.maps.interfaces.MapObject;

public class Layer extends AbstractLayer {
    private List<MapObject> elements;

    public Layer(String name) {
        super(name);
    }

    public Layer(String name, String description) {
        super(name, description);
    }

    public Layer(String name, Style style) {
        super(name, style);
    }

    public Layer(String name, String description, Style style) {
        super(name, description, style);
    }

    public Layer(LayerGroup parent, String name) {
        super(parent, name);
    }

    public Layer(LayerGroup parent, String name, Style style) {
        super(parent, name, style);
    }

    public Layer(LayerGroup parent, String name, String description, Style style) {
        super(parent, name, description, style);
    }

    public List<MapObject> getElements() {
        return elements;
    }

    public void setElements(List<MapObject> elements) {
        this.elements = elements;
    }

    public Layer add(MapObject element) {
        element.setLayer(this);
        elements = add(elements, element);
        return this;
    }
}
