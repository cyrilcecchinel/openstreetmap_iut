/*
 * ************************************************************************
 *                  Université de Nice Sophia-Antipolis (UNS) -
 *                  Centre National de la Recherche Scientifique (CNRS)
 *                  Copyright © 2016 UNS, CNRS
 *
 *   This library is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU Lesser General Public
 *   License as published by the Free Software Foundation; either
 *   version 3 of the License, or (at your option) any later version.
 *
 *   This library is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *   Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public
 *   License along with this library; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *
 *
 *     Author: Cyril Cecchinel – Laboratoire I3S – cecchine@i3s.unice.fr
 * ***********************************************************************
 */

// License: GPL. For details, see Readme.txt file.
package fr.unice.iut.info.methodo.maps.checkBoxTree;

import java.awt.BorderLayout;
import java.awt.Insets;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Node Panel for checkBox Tree
 *
 * @author galo
 */
public class CheckBoxNodePanel extends JPanel {
    /** Serial Version UID */
    private static final long serialVersionUID = -7236481597785619029L;
    private final JLabel label = new JLabel();
    private CheckBoxNodeData data;
    public final JCheckBox check = new JCheckBox();

    public CheckBoxNodePanel() {
        this.check.setMargin(new Insets(0, 0, 0, 0));
        setLayout(new BorderLayout());
        add(check, BorderLayout.WEST);
        add(label, BorderLayout.CENTER);
    }

    public void setSelected(Boolean bool) {
        if (bool == null) {
            check.getModel().setPressed(true);
            check.getModel().setArmed(true);
        } else {
            check.setSelected(bool.booleanValue());
            check.getModel().setArmed(false);
        }
    }

    public CheckBoxNodeData getData() {
        data.setSelected(check.isSelected());
        return data;
    }

    public void setData(CheckBoxNodeData data) {
        this.data = data;
        label.setText(data.getText());
    }

    public JLabel getLabel() {
        return label;
    }
}
